.. _structure:

Object structure
================
Living Labs/TREC OpenSearch internally uses a structure of interlinked objects that represent
documents, queries, feedback, etc. It is good to understand the connections between
the objects, if you want to troubleshoot or develop new functionality.

.. image:: uml-ll-structure.png
