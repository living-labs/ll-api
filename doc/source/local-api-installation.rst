Local API installation
======================


.. toctree::
        :maxdepth: 3
        :numbered:

        api-installation-tutorial
        api-installation-advanced
