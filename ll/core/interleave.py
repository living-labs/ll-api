import random


def team_draft_interleave(ranking_a, ranking_b, max_length, name_a="a",
                          name_b="b"):
    """
    Team-draft interleaving implementation
    
    :param ranking_a: The first ranking, an array of string identifiers
    :param ranking_b: The second ranking, an array of string identifiers
    :param max_length: The maximum length of the result list
    :param name_a: The team name of the first ranking
    :param name_b: The team name of the second ranking
    :return: The interleaved result list
    """
    result = []
    result_set = set([])

    pointer_a = 0
    pointer_b = 0

    length_ranking_a = len(ranking_a)
    length_ranking_b = len(ranking_b)

    length_a = 0
    length_b = 0

    while pointer_a < length_ranking_a and pointer_b < length_ranking_b and len(result) < max_length:
        if length_a < length_b or (length_a == length_b and bool(random.getrandbits(1))):
            result.append({'site_docid': ranking_a[pointer_a], 'team': name_a})
            result_set.add(ranking_a[pointer_a])
            length_a += 1
        else:
            result.append({'site_docid': ranking_b[pointer_b], 'team': name_b})
            result_set.add(ranking_b[pointer_b])
            length_b += 1
        while pointer_a < length_ranking_a and ranking_a[pointer_a] in result_set:
            pointer_a += 1
        while pointer_b < length_ranking_b and ranking_b[pointer_b] in result_set:
            pointer_b += 1
    return result
