# This file is part of Living Labs Challenge, see http://living-labs.net.
#
# Living Labs Challenge is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Living Labs Challenge is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Living Labs Challenge. If not, see <http://www.gnu.org/licenses/>.

from flask import request
from flask_restful import fields
from .. import api
from .. import core
from .. import ApiResource
from .. import requires_auth

doc_fields = {
    "site_docid": fields.String(),
}

doclist_fields = {
    "sid": fields.String(),
    "creation_time": fields.DateTime(),
    "doclist": fields.Nested(doc_fields)
}


class Interleave(ApiResource):
    @requires_auth
    def post(self, key, site_qid):
        """
        Obtain an interleaved ranking for a query.

        Every time this endpoint is called, a ranking produced by participants
        of the Challenge is selected based on a least-served basis.
        Due to this behavior, the ranking is likely to change for each call.
        Therefor, the site should perform caching on their own in order to show
        users stable rankings for repeated queries.
        
        The ranking is interleaved with the provided production ranking using
        teamdraft interleaving.

        The API will ensure that only documents that are presented in the most
        recent doclist for the requested query are returned.
        Sites are not expected to filter the ranking.
        If filtering is required for this query, please do so by updating the
        doclist.
        While we should aim to prevent this, it may happen that the site needs
        to make a last minute decision not to include a certain document.
        Make sure to incorporate this decision in the feedback.

        The site is expected to expose the retrieved interleaved ranking to a
        user and return user feedback :http:put:`/api/site/feedback/(key)/(sid)`
        as soon as it is available.

        .. note::

            Note the session id (sid) which will need to be stored on the sites
            end and should be returned as part of the feedback.


        :param site_qid: the site's query identifier
        
        :reqheader Content-Type: application/json
        :content:
            .. sourcecode:: javascript

                {
                    "doclist": [
                        {"site_docid": "4922d3c4fdb24296a90a20bdd20e"},
                        {"site_docid": "af1594296a90da20bdd20e40e737"},
                        {"site_docid": "b5ee9b2e327493c4fdb24296a94a"},
                    ]
                }
        
        :status 403: invalid key
        :status 404: query does not exist
        :return:
            .. sourcecode:: javascript

                {
                    "sid": "s1",
                    "doclist": [
                        {"site_docid": "4922d3c4fdb24296a90a20bdd20e",
                         "team": "site"},
                        {"site_docid": "91242f0da0a9037ddeb7e924056a",
                         "team": "participant"},
                        {"site_docid": "b5ee9b2e327493c4fdb24296a94a",
                         "team": "participant"},
                    ]
                }

        """
        site_id = self.get_site_id(key)
        json = request.get_json(force=True)
        self.check_fields(json, ["doclist"])

        # Get a ranking for the participant
        participant_run = self.trycall(core.run.get_ranking, site_id,
                                           site_qid)

        if len(json['doclist']) == 0:
            self.abort(400, "You need to provide at least one document in the "
                            "doclist")
        else:
            for e in json['doclist']:
                if not 'site_docid' in e:
                    self.abort(400, "You need to specify a site_docid for each "
                                    "document in the doclist")

        # Compute the interleaved ranking of the participant's ranking and the
        # provided site's ranking
        participant_ranking = [e['site_docid'] for e in participant_run['doclist']]
        site_ranking = [e['site_docid'] for e in json['doclist']]
        interleaved_ranking = self.trycall(core.interleave.team_draft_interleave,
            ranking_a=participant_ranking, ranking_b=site_ranking,
            max_length=len(site_ranking), name_a="participant", name_b="site")

        # Return the interleaved ranking and the corresponding session id
        return {
            'sid': participant_run['sid'],
            'doclist': interleaved_ranking
        }


api.add_resource(Interleave, '/api/v2/site/interleave/<site_qid>',
                 endpoint="site/interleave")
